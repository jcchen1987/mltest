#ifndef _PAGE_RANK_H_
#define _PAGE_RANK_H_

//Ref:  http://zh.wikipedia.org/wiki/PageRank
typedef unsigned char BYTE;
class CPageRank
{
public:
    CPageRank(int nWebNum = 5, bool bLoadFromFile = false);
    ~CPageRank();

    int Process();
    float *GetWeight();


private:
    void InitGraph(bool bLoadFromFile = false);
    void GenerateP();
    BYTE *m_pu8Relation; //节点关系 i行j列表示j是否指向i
    float *m_pf32P; //转移矩阵
    float *m_pf32Weight0;
    float *m_pf32Weight1;
    int *m_pl32Out;
    int m_nNum;
    float m_f32DampFactor; //阻尼系数

    int m_nMaxIterTime;
    float m_f32IterErr;

    float *m_pf32OutWeight;


};


#endif