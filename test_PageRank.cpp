#include <stdio.h>
#include <Windows.h>
#include "PageRank.h"

void test_PageRank()
{
    CPageRank *pPagerank = NULL;
    int nNum = 10;
    for(int i = 0; i < 100; i++)
    {
        pPagerank = new CPageRank(nNum, false);
        int t = pPagerank->Process();
        float *pf32Weight = pPagerank->GetWeight();

        printf("%d : Ittime[%d]", i, t);
        for(int j = 0; j < nNum; j++)
        {
            printf("  %.2f ", pf32Weight[j]);
        }
        printf("\n");

        if(pf32Weight[0] > 100 || pf32Weight[0] < 0.00000001)
        {
            break;
        }
        Sleep(1000);

        delete pPagerank;
    }
}