#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <stdio.h>
#include "PageRank.h"

#define OPENCV_SHOW
#ifdef OPENCV_SHOW
#include "opencv247/opencv2/opencv.hpp"
using namespace cv;

#define CV_VERSION_ID CVAUX_STR(CV_MAJOR_VERSION) CVAUX_STR(CV_MINOR_VERSION) CVAUX_STR(CV_SUBMINOR_VERSION)
#ifdef _DEBUG
#define cvLIB(name) "lib/opencv_" name CV_VERSION_ID "d"
#else
#define cvLIB(name) "lib/opencv_" name CV_VERSION_ID
#endif

#pragma comment( lib, cvLIB("core"))
#pragma comment( lib, cvLIB("highgui"))

Mat m_Img;
Point apt[1000] = {0};

void drawArrow(cv::Mat& img, cv::Point pStart, cv::Point pEnd, int len, int alpha,             
               const Scalar& color, int thickness, int lineType)
{    
    const double PI = 3.1415926;    
    Point arrow;    
    //计算 θ 角（最简单的一种情况在下面图示中已经展示，关键在于 atan2 函数，详情见下面）   
    double angle = atan2((double)(pStart.y - pEnd.y), (double)(pStart.x - pEnd.x));  
    line(img, pStart, pEnd, color, thickness, lineType);   
    //计算箭角边的另一端的端点位置（上面的还是下面的要看箭头的指向，也就是pStart和pEnd的位置） 
    arrow.x = pEnd.x + len * cos(angle + PI * alpha / 180);     
    arrow.y = pEnd.y + len * sin(angle + PI * alpha / 180);  
    line(img, pEnd, arrow, CV_RGB(255,255,255), thickness, lineType);   
    arrow.x = pEnd.x + len * cos(angle - PI * alpha / 180);     
    arrow.y = pEnd.y + len * sin(angle - PI * alpha / 180);    
    line(img, pEnd, arrow, CV_RGB(255,255,255), thickness, lineType);
}
#endif


CPageRank::CPageRank(int nWebNum, bool bLoadFromFile)
{
    m_f32DampFactor = 0.85;
    m_nMaxIterTime = 1000;
    m_f32IterErr = 1e-6;

    m_nNum = nWebNum;
    m_pu8Relation = new BYTE[m_nNum * m_nNum];
    m_pf32P = new float[m_nNum * m_nNum];
    m_pf32Weight0 = new float[m_nNum];
    m_pf32Weight1 = new float[m_nNum];
    m_pl32Out = new int[m_nNum];//每个节点指向的节点数

    InitGraph(bLoadFromFile);  
}

CPageRank::~CPageRank()
{
    delete []m_pl32Out;
    delete []m_pf32P;
    delete []m_pf32Weight0;
    delete []m_pf32Weight1;
    delete []m_pu8Relation;
}

int CPageRank::Process()
{
    int i,j,k,t;
    float f32MaxErr = 0.0f;
    float *pf32Org = m_pf32Weight0;
    float *pf32New = m_pf32Weight1;
    float f32MinWeight = (1 - m_f32DampFactor) / m_nNum;

    //设置初始值，全1
    for(i = 0; i < m_nNum; i++)
    {
        pf32Org[i] = 1.0f / m_nNum;//rand() * 2.0f / RAND_MAX;
    }

    GenerateP();

#ifdef OPENCV_SHOW
    char as8[256] = {0};
    Mat show = m_Img.clone();
    for(i = 0; i < m_nNum; i++)
    {
        sprintf(as8, "ID[%d]-W[%.8f]", i, pf32Org[i]);
        int x,y;
        x = apt[i].x < 250 ? apt[i].x + 5 : apt[i].x - 50;
        y = apt[i].y < 250 ? apt[i].y + 5 : apt[i].y - 5;
        putText(show, as8, Point(x,y), CV_FONT_HERSHEY_COMPLEX, 0.5, Scalar(255, 255, 0));
    }
    imshow("res", show);
    waitKey(10);
#endif
    //迭代
    for(t = 0; t < m_nMaxIterTime; t++)
    {
        //开始迭代
        f32MaxErr = 0.0f;
        for(i = 0; i < m_nNum; i++)
        {
            pf32New[i] = f32MinWeight;
            int l32Off = i * m_nNum;
            for(j = 0; j < m_nNum; j++)
            {
                pf32New[i] += m_pf32P[l32Off + j] * pf32Org[j];
            }

            float f32Err = fabs(pf32New[i] - pf32Org[i]);
            if(f32Err > f32MaxErr)
            {
                f32MaxErr = f32Err;
            }
        }
#ifdef OPENCV_SHOW
        char as8[256] = {0};
        Mat show = m_Img.clone();
        for(i = 0; i < m_nNum; i++)
        {
            sprintf(as8, "ID[%d]-W[%.8f]", i, pf32New[i]);
            int x,y;
            x = apt[i].x < 250 ? apt[i].x + 5 : apt[i].x - 50;
            y = apt[i].y < 250 ? apt[i].y + 5 : apt[i].y - 5;
            putText(show, as8, Point(x,y), CV_FONT_HERSHEY_COMPLEX, 0.5, Scalar(255, 255, 0));
        }
        sprintf(as8, "%d", t);
        putText(show, as8, Point(30,30), CV_FONT_HERSHEY_COMPLEX, 0.8, Scalar(0, 255, 0));

        imshow("res", show);
        waitKey(1);
#endif
        //迭代误差足够小，停止
        if(f32MaxErr < m_f32IterErr)
        {
            break;
        }

        //交换2次迭代结果
        float *pf32Temp = pf32Org;
        pf32Org = pf32New;
        pf32New = pf32Temp;
    }

    //迭代结果存在pf32New中
    m_pf32OutWeight = pf32New;
#ifdef OPENCV_SHOW
    waitKey(0);
#endif
    return t;
}

void CPageRank::InitGraph(bool bLoadFromFile)
{
    FILE *pf = NULL;
    if(bLoadFromFile)
    {
        pf = fopen("map.dat", "rb");
        if(pf)
        {
            fread(m_pu8Relation, sizeof(BYTE), m_nNum * m_nNum, pf);
            fclose(pf);
            goto Finish_Create_Label;
        }
    }

    //建立随机的节点指向图
    int i, j;
    srand((unsigned)time(NULL));
    for(i = 0; i < m_nNum; i++)
    {
        //指向第i个的节点
        for(j = 0; j < m_nNum; j++)
        {
            m_pu8Relation[i * m_nNum + j] = rand() & 1;
        }
        //自己不指向自己
        m_pu8Relation[i * m_nNum + i] = 0;
    }

    pf = fopen("map.dat", "wb");
    if(pf)
    {
        fwrite(m_pu8Relation, sizeof(BYTE), m_nNum * m_nNum, pf);
        fclose(pf);
    }
    
Finish_Create_Label:
    int k = 0;

#ifdef OPENCV_SHOW
    namedWindow("res", 0);
    int w = 600;
    int h = 500;
    int cx = 250;
    int cy = 250;
    int radius = 200;
    float angle = 2 * 3.1415 / m_nNum;
    m_Img = Mat::zeros(h,w,CV_8UC3);

    for(i = 0; i < m_nNum; i++)
    {
        apt[i].x = cx + radius * cos(angle * i);
        apt[i].y = cy + radius * sin(angle * i);
        circle(m_Img, apt[i], 3, CV_RGB(0,255,0));
    }

    BYTE *pu8 = m_pu8Relation;
    for(i = 0; i < m_nNum; i++)
    {
        for(j = 0; j < m_nNum; j++)
        {
            if(*pu8 > 0)
            {                
                drawArrow(m_Img, apt[j], apt[i], 15, 20, CV_RGB(255,0,255), 1, 8);
            }
            pu8++;
        }
    }

    imshow("res", m_Img); 
#endif
   
}

void CPageRank::GenerateP()
{
    int i,j;
    float *pf32P = NULL;
    BYTE *pu8Relation = NULL;

    //统计流入流出每个节点数
    memset(m_pl32Out, 0, m_nNum * sizeof(int));
    pu8Relation = m_pu8Relation;
    for(i = 0; i < m_nNum; i++)
    {
        for(j = 0; j < m_nNum; j++)
        {
            m_pl32Out[j] += *pu8Relation;
            pu8Relation++;
        }
    }

    //生成转移矩阵,每个节点的权重平均流出
    pu8Relation = m_pu8Relation;
    pf32P = m_pf32P;
    for(i = 0; i < m_nNum; i++)
    {
        for(j = 0; j < m_nNum; j++)
        {
            if(m_pl32Out[j] > 0)
            {
                *pf32P = *pu8Relation * 1.0f / m_pl32Out[j];
            }
            else
            {
                *pf32P = 0.0f;
            }
            pu8Relation++;
            pf32P++;
        }
    }

    //考虑阻尼系数，修正转移矩阵
    pf32P = m_pf32P;
    for(i = 0; i < m_nNum; i++)
    {
        for(j = 0; j < m_nNum; j++)
        {
            *pf32P = *pf32P * m_f32DampFactor;
            pf32P++;
        }
    }
}



float * CPageRank::GetWeight()
{
    return m_pf32OutWeight;
}